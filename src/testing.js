import React from 'react'
import {render, fireEvent} from '@testing-library/react'
import App from './App'

test('counter increments the counter', () => {
	const {container} = render (<App />)
	const button = container.firstChild
	expect(button.textContent).toBe('0')
	fireEvent.click(button)
	expect(button.textContent).toBe('1')
	
})	