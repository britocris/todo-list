import React, {useState} from 'react';
import {Container, Button } from 'reactstrap'
import './App.css';

const initialState = {
  inputValue: "",
  edit: false,
  editChild: 0,
  todos: [
    {name: "Lavar", tacched: "none", textColor: "none", checked: false}, 
    {name: "Planchar", tacched: "none", textColor: "none", checked: false},
    {name: "Barrer", tacched: "none", textColor: "none", checked: false},
    {name: "Coletear", tacched: "none", textColor: "none", checked: false}],
}

function App () {

  const [state, setState] = useState(initialState)
  const { inputValue, todos, edit, editChild } = state

  function handlerInputChange(e) {
    setState({...state, inputValue: e.target.value})
  }

  function AddToDo() {
    if(inputValue !== "") {
      if(!edit)
        setState({...state, todos: [...todos, {name: inputValue, tacched: "none", textColor: "none", checked: false}], inputValue: "" })
      else  
        setState({
          ...state,
          todos: todos.map((todo, i) => {
            if(i === editChild) {
              return {...todo, name: inputValue}
            } else return todo
          }),
          inputValue: "",
          edit: false
        })
    }
    else {
      alert("Debe agregar una tarea")
    }
  }

  function deleteToDo(index) {
    if(todos[index].name === inputValue) 
      setState({
        ...state,
        todos: todos.filter((todo, i) => i !== index ),
        inputValue: "",
        edit: false,
        editChild: 0
      })
    else 
      setState({
        ...state,
        todos: todos.filter((todo, i) => i !== index )
      })
  }

  function keyPressed(e) {
    if(e.charCode === 13) {
      AddToDo()
    }
  }

  function editToDo(index) {
    setState({
      ...state, inputValue: todos[index].name, edit: true, editChild: index
    })
  }

  function checkToDo(index) {

    setState({
      ...state,
      todos: todos.map((todo, i) => {
        if(i===index) {
          if(todo.tacched === "line-through") {
            return {...todo, tacched: "none", textColor: "none", checked:false }
          }
          if(todo.tacched === "none") {
            return {...todo, tacched: "line-through", textColor: "red", checked: true}
          }
        } else return todo
      })
    })
  }

  return (
    <Container>
      <div style={{display:"flex", justifyContent: "space-around", width: "40%"}} action="">
        <label htmlFor="">Add ToDo</label>
        <input onKeyPress={ (e) => keyPressed(e) } onChange={(e) => handlerInputChange(e)} name="inputValue" value={inputValue} type="text"/>
        <Button color="info" onClick={AddToDo} type="button"> {edit ? "Edit" : "Add"} </Button>
      </div>
      <div style={{width: "40%"}}>
        <ul style={{paddingLeft: 0}}>
          {todos && todos.map((todo, index) => (
            <li style={{listStyleType: "none"}} key={index}>
              <div style={{display: "flex", marginTop: "10px", marginBottom: "10px", justifyContent: "space-between"}}>
                <input onChange={ () => checkToDo(index) } checked={todo.checked} type="checkbox"/>
                <label onClick={ () => editToDo(index) } id="todoText" style={{textDecoration: todo.tacched, textDecorationColor: todo.textColor}} htmlFor=""> {todo.name} </label>
                <Button color="danger" onClick={() => deleteToDo(index)} type="button"> Delete </Button>
              </div>
            </li>
          ))}
        </ul>
      </div>
    </Container>
  );
}

export default App;
